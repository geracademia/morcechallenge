﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApi.Controllers
{
    public class TraductorController : ApiController
    {
        [HttpGet]
        public string Traslate2Text(string texto)
        {
            return TraductorUtil.TraductorUtil.translate2Human(texto);
        }

        [HttpGet]
        public string Traslate2Morse(string texto)
        {
            return TraductorUtil.TraductorUtil.TextoAMorse(texto);
        }

        [HttpGet]
        public string decodeBits2Morse(string texto)
        {
            return TraductorUtil.TraductorUtil.decodeBits2Morse(texto);
        }
    }
}
