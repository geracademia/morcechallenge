﻿using System;
using System.Collections.Generic;

namespace TraductorUtil
{
    public static class TraductorUtil
    {

        /// <summary>
        /// Recibe un texto de en codigo Morse y lo traduce a texto
        /// </summary>
        /// <param name="texto"> Cadena en codigo morse </param>
        /// <returns> Cadena traducida</returns>
        public static string translate2Human(string texto)
        {
            // Armo el diccionario con las duplas Codigo Morse/ Caracter
            Dictionary<string, string> caracteres = new Dictionary<string, string>
            {
                { ".-", "A" },{ "-...", "B" },{ "-.-.", "C" },{ "-..", "D" },{ ".", "E" },{ "..-.", "F" },{ "--.", "G" },
                { "....", "H" },{ "..", "I" },{ ".---", "J" },{ "-.-", "K" },{ ".-..", "L" },{ "--", "M" },{ "-.", "N" },
                { "---", "O" },{ ".--.", "P" },{ "--.-", "Q" },{ ".-.", "R" },{ "...", "S" },{ "-", "T" },{ "..-", "U" },
                { "...-", "V" },{ ".--", "W" },{ "-..-", "X" },{ "-.--", "Y" },{ "--..", "Z" },{ "-----", "0" },{ ".----", "1" },
                { "..---", "2" },{ "...--", "3" },{ "....-", "4" },{ ".....", "5" },{ "-....", "6" },{ "--...", "7" },{ "---..", "8" },
                { "----.", "9" },{ "$", " " },
            };

            string resultado = "";
            // Detecto las letras al dividir el texto usando el espacio
            string[] letras = DetectarEspacios(texto).Split(Convert.ToChar(" "));

            // Utilizando cada division busco realizo la traducción de código a caracter
            foreach (string item in letras)
            {
                // si se corresponde a un caracter lo agrego a la cadena de retorno
                if (caracteres.ContainsKey(item))
                    resultado += caracteres[item];
            }

            return resultado;
        }

        /// <summary>
        /// Recibe un texto y lo convierte en codigo morse
        /// </summary>
        /// <param name="texto"></param>
        /// <returns></returns>
        public static string TextoAMorse(string texto)
        {
            Dictionary<string, string> caracteres = new Dictionary<string, string>
            {
                {"A",".-"},{"B","-..."},{"C","-.-."},{"D","-.."},{"E","."},{"F","..-."},{"G","--."},
                {"H","...."},{"I",".."},{"J",".---"},{"K","-.-"},{"L",".-.."},{"M","--"},{"N","-."},
                {"O","---"},{"P",".--."},{"Q","--.-"},{"R",".-."},{"S","..."},{"T","-"},{"U","..-"},
                {"V","...-"},{"W",".--"},{"X","-..-"},{"Y","-.--"},{"Z","--.."},{"0","-----"},{"1",".----"},
                {"2","..---"},{"3","...--"},{"4","....-"},{"5","....."},{"6","-...."},{"7","--..."},
                { "8","---.."},{"9","----."},{"$"," "}
            };

            string resultado = "";

            foreach (char item in texto.Replace(" ", "$").ToUpper().ToCharArray())
            {
                if (caracteres.ContainsKey(item.ToString()))
                    resultado += caracteres[item.ToString()] + " ";
                else if (item.ToString() == " ")
                    resultado += caracteres[item.ToString()] + " ";
            }

            return resultado;
        }

        /// <summary>
        /// Detecta si hay espacios en la oración a traducir, si los hay los marca con "$"
        /// </summary>
        /// <param name="texto"></param>
        /// <returns></returns>
        private static string DetectarEspacios(string texto)
        {
            string resultado = "";
            bool flagEspacio = true;
            // Recorro el texto
            for (int i = 0; i < texto.Length; i++)
            {
                // Si el caracter actual es espacio y el proximo tambien, concateno "$" para saber que es
                // un espacio en la traducción, y cambio el flag para que ignore los siguientes espacios
                // hasta que se detecte una letra
                if (texto.Length > i && texto[i].ToString() == " " && texto[i + 1].ToString() == " " && flagEspacio)
                {
                    resultado += " $ ";
                    flagEspacio = false;
                }
                else
                {
                    resultado += texto[i].ToString();
                    flagEspacio = true;
                }
            }

            return resultado;
        }

        public static string decodeBits2Morse(string texto)
        {
            // NOTA: En la secuencia de ejemplo la separación de caracter y el espacio entre letras no permite
            // descifrar el espacio en el texto

            string resultado = "";
            // Tomo el primer caracter para detectar cuando la secuencia cambia de 0 a 1
            Char ultimoCaracter = texto[0];
            string cadenaParcial = "";
            for (int i = 0; i < texto.Length; i++)
            {
                // Si el caracter es igual al ultimo, continuo censando, agrupo las secuencias de bit
                // iguales consecutivos
                if (texto[i] == ultimoCaracter)
                    cadenaParcial += texto[i].ToString();
                // Si el Bit cambio significa que termino el caracter o el espacio
                else
                {
                    // Si la secuencia es de unos y mas corta que 4 lo traduzco como Punto
                    if (cadenaParcial.Contains("1") && cadenaParcial.Length < 4)
                        cadenaParcial = ".";

                    // Si la secuencia es de unos y mas larga que 4 lo traduzco como Guion
                    else if (cadenaParcial.Contains("1") && cadenaParcial.Length > 4)
                        cadenaParcial = "-";

                    // Si la secuencia es de ceros y mas larga que 3 lo traduzco como espacio
                    else if (cadenaParcial.Contains("0") && cadenaParcial.Length > 3)
                        cadenaParcial = " ";
                    
                    // caso contrario lo ignoro
                    else
                        cadenaParcial = "";

                    resultado += cadenaParcial;
                    ultimoCaracter = texto[i];
                    cadenaParcial = texto[i].ToString();
                }
            }

            return resultado;
        }
    }
}
