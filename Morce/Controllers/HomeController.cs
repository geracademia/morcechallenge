﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Morse.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Esta página permite la traducción de Bit a código morse y de texto a código morse.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Contacto a ariasgermanpablo@gmail.com";
            return View();
        }
    }
}