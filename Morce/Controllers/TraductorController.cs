﻿using Morse.Models;
using System.Web.Mvc;

namespace Morse.Controllers
{
    public class TraductorController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(TraductorModel model, string command)
        {
            if (command == "translate2Human")
            {
                model.Resultado = TraductorUtil.TraductorUtil.translate2Human(model.TextoOrigen);
            }
            if (command == "decodeBits2Morse")
            {
                model.Resultado = TraductorUtil.TraductorUtil.decodeBits2Morse(model.TextoOrigen);
            }

            return View(model);
        }

    }
}